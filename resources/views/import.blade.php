<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-6 mt-5">
                <center>
                    <h3 style="font-family:carsive;">Import & Export Excel Sheet 🤠🧾</h3>
                </center>
                <div class="p-5 border-2 border border-secondary rounded">
                    <form action="{{URL::to('/importFile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        Select excel file :
                        <input type="file" name="uploadFile" class="form-control"  required value="" /><br><br>
                        <div class="mt-5">
                            <button type="submit" class="btn btn-info" name="submit" value="Upload">Upload</button>
                            <a href="{{ URL::to('/exportFile')}}" class="btn btn-primary fload-right"> Export Excel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <h2 style="font-family:cursive; color:darkolivegreen">All Data</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Pro-Category</th>
                        <th>Pro-Name</th>
                        <th>Pro-Price</th>
                    </tr>
                </thead>
                <tbody> <?php $index = 1; ?>
                    <?php foreach ($data as $row) { ?>
                        <tr>
                            <td>{{$index++}}</td>
                            <td>{{$row->catName}}</td>
                            <td>{{$row->subCatName}}</td>
                            <td>{{$row->proName}}</td>
                            <td>{{$row->proPrice}}</td>
                        </tr><?php } ?>
                </tbody>
            </table>
        </div>
</body>

</html>