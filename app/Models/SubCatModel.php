<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCatModel extends Model
{
    use HasFactory;
    protected $table = "tblsubcat";
    protected $fillable = [
        'subCatName',
        'catId'
    ];
    public  $timestamps = false;
}
