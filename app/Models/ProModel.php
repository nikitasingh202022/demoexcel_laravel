<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProModel extends Model
{
    use HasFactory;
    protected $table = "tblpro";
    protected $fillable = [
        'catId',
        'subCatId',
        'proName',
        'proPrice'
    ];
    public  $timestamps = false;
}
