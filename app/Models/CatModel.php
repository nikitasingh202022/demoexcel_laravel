<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatModel extends Model
{
    use HasFactory;
    protected $table = "tblcat";
    protected $fillable = [
        'catId',
        'catName',
    ];
    public  $timestamps=false;
}
