<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllData extends Model
{
    use HasFactory;
    protected $table = 'tblalldata';
    protected $fillable = [
        'catName',
        'subCatName',
        'proName',
        'proPrice'
    ];
    public  $timestamps = false;
}
