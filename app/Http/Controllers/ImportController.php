<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Imports\ExcelImport;
use App\Imports\UsersImport;
use Exception;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportController extends Controller
{
    public function import()
    {
        // $data['data'] = DB::table('tblalldata')->select('*')->get();
        $sql = "SELECT tblpro.*, tblcat.catId, tblcat.catName, tblsubcat.subCatId, tblsubcat.subCatName 
        FROM tblpro
        RIGHT JOIN tblcat ON tblpro.catId = tblcat.catId
        RIGHT JOIN tblsubcat ON tblpro.subCatId = tblsubcat.subCatId
        WHERE tblpro.proId ";
        $data['data'] = DB::select($sql);
        return view('/import', $data);
        // return view('import');
    }
    public function exportFile()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function ExportExcel($customer_data)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '4000M');

        try {
            $spreadSheet = new Spreadsheet();
            $spreadSheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
            $spreadSheet->getActiveSheet()->fromArray($customer_data);
            $Excel_writer = new Xls($spreadSheet);
            header('Content-Type: users.xlsx');
            dd(header('Content-Type: users.xlsx'));

            header('Content-Disposition: attachment;filename="Customer_ExportedData.xls"');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            $Excel_writer->save('');
            exit();
        } catch (Exception $e) {
            return;
        }
    }


    public function importFile(Request $request)
    {

        try {
            $path = $request->file('uploadFile')->getRealPath();
            $file = $request->file('uploadFile');
            Excel::import(new ExcelImport, $file);
            return redirect('/import')->with('Users imported successfully!');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
    public function getData()
    {
        $data['data'] = DB::table('tblalldata')->select('*')->get();
        return view('/import', $data);
    }
}
