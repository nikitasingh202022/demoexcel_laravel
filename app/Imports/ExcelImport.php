<?php

namespace App\Imports;

use App\Models\AllData;
use App\Models\CatModel;
use App\Models\ProModel;
use App\Models\SubCatModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
   public function collection(Collection $collection)
{
    foreach ($collection as $row) {
        $allData = AllData::create([
            'catName' => $row[0],
            'subCatName' => $row[1],
            'proName' => $row[2],
            'proPrice' => $row[3],
        ]);

        $this->shareData($allData);
    }
}

public function shareData($allData)
{
    if ($allData['catName'] != '') {
        $category = CatModel::where('catName', $allData['catName'])->first();

        if (!$category) {
            $category = CatModel::create([
                'catName' => $allData['catName'],
            ]);
            $catId = $category->id;
        }
        else{
            $catId = $category->catId;

        }

    }

    if ($allData['subCatName'] != '') {
        $subcategory = SubCatModel::where('subCatName', $allData['subCatName'])->first();

        if (!$subcategory) {
            $subcategory = SubCatModel::create([
                'subCatName' => $allData['subCatName'],
                'catId' => $catId,
            ]);
            $subCatId = $subcategory->id;
        }
        else{
            $subCatId = $subcategory->subCatId;
        }

        
    }

    if ($allData['proName'] != '' || $allData['proPrice'] != '') {
        $product = ProModel::where([
            'proName' => $allData['proName'],
            'proPrice' => $allData['proPrice'],
            'catId' => $catId,
            'subCatId' => $subCatId,
        ])->first();

        if (!$product) {
            $product = ProModel::create([
                'proName' => $allData['proName'],
                'proPrice' => $allData['proPrice'],
                'catId' => $catId,
                'subCatId' => $subCatId,
            ]);
        }
    }
}




    # Methode 1
    // dd($row);
    // $cat = CatModel::insertGetId([
    //     'catName' => $row[0],
    // ]);
    // $catId = $cat->id;
    // dd($catId);
    // $subCat = SubCatModel::insertGetId([
    //     'catName' => $catId,
    //     'subCatName' => $row[1],
    // ]);
    // $subCatId = $subCat->catId;
    // $product = ProModel::insertGetId([
    //     'proName' => $row[2],
    //     'proPrice' => $row[3],
    //     'catId' => $catId,
    //     'subCatId' => $subCatId,
    // ]);

    // dd($collection);
    // $cat = new CatModel();
    // $cat->catName = $row[0];
    // $cat->save();
    // $catId = $cat->id;

    // $subCat = new SubCatModel();
    // $subCat->subCatName = $row[1];
    // $subCat->catId = $catId;
    // $subCat->save();
    // $subCatId = $subCat->id;

    // $pro = new ProModel();
    // $pro->proName = $row[2];
    // $pro->proPrice = $row[3];
    // $pro->catId = $catId;
    // $pro->subCatId = $subCatId;
    // $pro->save();
    // $proId = $pro->id;
    // dd($pro->catId = $catId);
}
