<?php

namespace App\Imports;

use App\Models\CatModel;
use App\Models\ProModel;
use App\Models\SubCatModel;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // return new User([
        //     'id' => $row[0],
        //     'catName' => $row[1],
        //     'subCatName' => $row[2],
        //     'proName' => $row[3],
        //     'proPrice' => $row[4]
        // ]);
        $table1Data = [
            'id' => $row[0],
            'catName' => $row[1],
            // other columns for table1
        ];

        $table2Data = [
            'subCatName' => $row[2],

            // other columns for table2
        ];
        $table3Data = [
            'proName' => $row[3],
            'proPrice' => $row[4],
            // other columns for table2
        ];

        CatModel::create($table1Data);
        SubCatModel::create($table2Data);
        ProModel::create($table3Data);
    }
}
// class EmployeesImport implements ToCollection
// {
//     public function collection(Collection $rows)
//     {
//         foreach ($rows as $row) 
//         {
//             EmployeeType::create([
//                 'name' => $row[1],
//             ]);

//             Contact::create([
//                 'institution' => $row[3],
//                 'type' => $row[4],
//                 'address' => $row[5],
//             ]);
//         }
//     }
// }