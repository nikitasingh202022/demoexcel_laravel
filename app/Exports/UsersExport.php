<?php

namespace App\Exports;

use App\Models\AllData;
use App\Models\CatModel;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return AllData::select('*')->get();
        // select('catName', 'subCatName', 'proName', 'proPrice')->get();
    }
}
