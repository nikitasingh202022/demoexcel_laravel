<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tblalldata', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catName');
            $table->string('subCatName');
            $table->string('proName');
            $table->string('proPrice');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tblalldata');
    }
};
