<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tblPro', function (Blueprint $table) {
            $table->increments('proId');
            $table->integer('catId')->default(NULL);
            $table->integer('subCatId')->default(NULL);
            $table->string('proName');
            $table->string('proPrice');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tblPro');
    }
};
